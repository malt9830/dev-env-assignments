# Lecture 3: Requirement Specifications

Below are assignments from the 3rd lecture of requirement specifications and knowledge management systems.

[TOC]

## Assignments

### Assignment 3a.1

_Make a template for a requirement specification. Put some thought into what columns it should have._


Template is found [here](https://docs.google.com/spreadsheets/d/1zRUIYke_p5LE9tDvMoFSc8yJcoUr2McqhmCE3cHGrBk/edit#gid=0&range=A1) and contains both the intended-to-use columns for the requirements specification as well as some columns originally supposed to be part of it.

#### Included Columns

The first 3 and simple columns are the *id*, *title* and *description*, simply giving it an easy point of reference, a name of reference and a general short description

Additionally a *type* column detailing the type of requirements specification, i.e. *business*, *user* or *system*. The *priority* column is based on the [*MoSCoW*](https://en.wikipedia.org/wiki/MoSCoW_method) method, descending from *must have* through *should have* to *could have* before reaching *won't have* for requirements whose exclusion has already been determined.

The *F/NF* column regards, whether the requirement is functional or non-functional, i.e. what the system should do and under which conditions and circumstances the functionality should work, respectively. The *ref* column is for non-functional requirements only, referring to its associated functional requirement.

#### Excluded Columns

Of the columns that were left out, are there *milestone* and *responsible*, both of which *could* be added as valid columns, be it in a general sense, i.e. *responsible* should refer to a role rather than a specific person and *milestone* should be generalised version of the *date* column, refering to which point a requirement should be met, before the project becomes a certain version, i.e. *conceptual product*, *minimum viable product*, *acceptance test* or *project complete*.

Aforementioned *date* is left out as we would wanna keep the specification as *static* as possible and something like setting a deadline would be very likely to be altered throughout the development process.

Likewise, the *status* column would be guaranteed to be changed, as the purpose of the kanban-board-like column would be changing it, as it progresses through *to do*, *in progress*, *done* and *approved*, maybe even *blocked*.

### Assignment 3a.2

_Fill out your requirement specification template for a project you are working on._

I filled out a requirements specification for my multimedia design final semester exam project, *Meefood*, which can be found [here](https://docs.google.com/spreadsheets/d/1zRUIYke_p5LE9tDvMoFSc8yJcoUr2McqhmCE3cHGrBk/edit#gid=240058462&range=A2) containing the columns mentioned in the *included* section above.

### Assignment 3b.1

_Fill out a requirement specification for a personal knowledge management system that you would use yourself._

The requirements specification for the knowledge management system was filled out similarly to the one above and can be found [here](https://docs.google.com/spreadsheets/d/1zRUIYke_p5LE9tDvMoFSc8yJcoUr2McqhmCE3cHGrBk/edit#gid=81002107&range=A1).
