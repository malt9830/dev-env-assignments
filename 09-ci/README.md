# Lecture 9: Continuous Integration

Below is the description of the initial implementation of a CI pipeline in the [Gamehub API](https://gitlab.com/malt9830/gamehub-api/).

[TOC]

## Gamehub API CI pipeline - static stage

As part of the lecture, an initial CI pipeline was created through a [`.gitlab-ci.yml`](https://gitlab.com/malt9830/gamehub-api/-/blob/master/.gitlab-ci.yml) and looks the following:

```yml
stages:
  - static

static:
  stage: static
  image: node:latest
  before_script:
    - npm install
  script:
    - npm run lint
```

It uses the latest node image as it is an Express app and then install dependencies via `npm install` and then it runs the lint command present in the `package.json` resolving the following:

```
"test:"eslint . --ext .js,.ts --fix --ignore-path .gitignore"
```