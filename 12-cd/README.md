# Lecture 12: Continuous Deployment

Below are the assignments from the 12th lecture, regarding the continuous deployment of the Docker image generated in the [Gamehub API CI pipeline](https://gitlab.com/malt9830/gamehub-api/-/blob/master/.gitlab-ci.yml).

[TOC]

## Assignments

### Assignment 12.1

_Install the at scheduler and have it schedule some tasks. The jobs will run in their own non-interactive shell, so you will not be able to see the tasks run directly in your shell. Have the jobs do things to some test files so you can tell if they run successfully._

Inside the `kea-dev` server, I installed the `at` package, and set up some sample `at` scheduels, e.g. `echo 'touch killroy-was-here' >> killroy.txt | at now +1 minutes` from the lecture notes.

### Assignment 12.2

_For a simple project, try to implement continuous deployment via push._

To update the image inside our `kea-dev` server, whenever a commit was pushed to GitLab, I expanded the [`.gitlab-ci.yml`](https://gitlab.com/malt9830/gamehub-api/-/blob/master/.gitlab-ci.yml?ref_type=heads) to include a *deploy* stage, and added several GitLab CI/CD pipeline variables.

#### The deploy stage

The snippet below includes comments detailing each step of *before_script*. The first 2 commands simply check for the existence of the ssh-agent, installing it, if it does not exist. Explanations for the other commands are found below in the variables sub-section.

```yml
deploy:
  stage: deploy
  image: ubuntu:latest
  before_script:
    # Check for existence of ssh-agent and install
    - 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    # Decode private key from base64 into .ssh directory as id_rsa
    - echo -n "$SSH_PRIVATE_KEY" | base64 -d | ssh-add -
    # Create .ssh directory with only owner permissions
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    # Use ssh-keyscan to scan the keys and grant permissions of read for all and write for owner
    - ssh-keyscan -H "$SERVER_HOST" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - ssh "$SSH_SERVER" "pull-gamehub-image"

```

#### CI pipeline variables

3 variables were created, those being `$SSH_PRIVATE_KEY`, `$SERVER_HOST` and `SSH_SERVER`.

The `$SSH_PRIVATE_KEY` variable contains an SSH private key that was generated alongside a public key which was added to the `authorized_keys` directory of the `kea-dev` server's `/.ssh`. Due to masking limitation for GitLab variables, the key was first encoded via `base64 -w 0`, hence the reason we pipe the variable to `base64 -d` decoding it before again passing it to `ssh-add -` using the SSH agent to add the key, allowing access to the `kea-dev` server.

Additionally, we use the `$SERVER_HOST` variable, this being the host name of the server, for our `ssh-keyscan`, retrieving host keys from the `kea-dev` server and appending those into the `known_hosts` in our `.ssh` directory, before granting the permissions of `644`, allowing the owner to read/write, while granting read-only permissions to others.

Lastly, we have the `$SSH_SERVER` variable that is combination of the `kea-dev` username and the hostname itself. Usually, I would be logging in via `malth@kea-dev` as the `config` in the `.ssh` directory, forwards *kea-dev* to the host name, but in the pipeline we simply explicitly use the host name which is given in the masked variable. Thereafter, the `pull-gamehub-image` script is called, pulling the most recent image that was built in the previous stage of the pipeline, that being the *build stage*.

### Assignment 12.3

_For a simple project, try to implement continuous deployment via pull._

For the *pull* iteration of the CD, I created a simple [Flask](https://flask.palletsprojects.com/en/3.0.x/) app as a webhook receiver on the `kea-dev` server that upon a POST request runs the same `pull-gamehub-image` as mentioned in the previous assignment.

#### Flask app webhook

The app creates 2 routes using Flask, a simple root and a `/webhook` route that upon a POST request, runs the the `handle-webhook.sh` script that in return runs the `pull-gamehub-image` shell script. Upon success, it returns a message saying the image was pulled succesfully, while if an error occurs, it returns that information instead.

```python
#!/usr/bin/env python3

from flask import Flask, request, jsonify
import subprocess

app = Flask(__name__)

@app.route('/')
def home():
    return '<p>Webhook</p>'

@app.route('/webhook', methods=['POST'])
def handle_webhook():
    try:
        subprocess.run(['./handle-webhook.sh'], check=True)
        return jsonify({'message': 'Image pulled succesfully'}), 200
    except subprocess.CalledProcessError as e:
        return jsonify({'message': 'Error pulling image'}), 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
```

#### The deploy stage

Thereafter I updated the *before_script* step of the *deploy* stage to install `curl` which we then in the *script* step use to send a POST request to the Flask app's route via a masked variable `$WEBHOOK_URL`. This would then trigger the webhook, running the `pull-gamehub-image` updating the image to the one built in the previous *build* stage.

```yml
deploy:
  stage: deploy
  image: ubuntu:latest
  before_script:
    # Install curl
    - apt-get update && apt-get install -y curl
  script:
    # Send a post request to the webhook
    - curl -X POST "$WEBHOOK_URL"
```