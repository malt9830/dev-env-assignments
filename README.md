# KEA Development Environments

This project includes development environment assignments as well as notes, demos and tables for commands, directives, flags, etc. related to the course.

Below are links to each lecture's directory and descriptions of what they contain.

[TOC]

## [Lecture 1: Introduction, Vim and Tmux](/01-intro)

This directory includes commands from Vimtutor as well as links to cheatsheets for [*Vim*](https://vim.rtorr.com/) and [*Tmux*](https://tmuxcheatsheet.com/).

## [Lecture 2: UNIX](/02-unix)

This directory includes scripts from the lecture's 6 assignments as well as descriptions of their functionality and implementation.

## [Lecture 3: Requirement Specifications](/03-reqspec)

This directory includes descriptions of the requirement specifications assignments and links to their [implementation](https://docs.google.com/spreadsheets/d/1zRUIYke_p5LE9tDvMoFSc8yJcoUr2McqhmCE3cHGrBk/edit#gid=0&range=A1) in Google Sheets.

## [Lecture 4: Version Control Systems](/04-git)

This directory includes descriptions of the assignments as well as the *Git by Example* demo.

## [Lecture 5: Virtualization and Process Isolation](/05-docker)

This directory includes the [*docker-demo*](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-05/#dockerfile-images-and-containers) from class and the [project](https://gitlab.com/malt9830/docker-todo) from the *getting-started* demo alongside descriptions of every step, as well as tables describing Docker commands, flags and directives.

## [Lecture 6: Virtualization and Process Isolation - Part II](/06-docker)

This directory includes the [*docker-compose-demo*](https://gitlab.com/malt9830/dev-env-assignments/-/tree/main/6-docker/docker-compose-demo) from the lecture and the [*helloweb-demo*](/6-docker/helloweb) from class, alongside a list of Docker compose related commands and the lecture's assignments.

## [Lecture 7: Workshop - Dockerize Your App](/07-docker-workshop)

This directory includes only a link to the [dockerized app](https://gitlab.com/malt9830/gamehub-api) whose project includes a descriptive README of its dockerization regarding the `Dockerfile` and `docker-compose.yml`.

## [Lecture 8: Software Quality Assurance](/08-sqa)

This directory includes answers to the first several assignments and a link to the final assignment's [project](/8-sqa/multiply) in which a README can be found detailing the app and its used SQA tools.

## [Lecture 9: Continuous Integration](/09-ci)

This directory includes only a README detailing the implementation a simple [CI pipeline](https://gitlab.com/malt9830/gamehub-api/-/blob/master/.gitlab-ci.yml) in the [Gamehub API project](https://gitlab.com/malt9830/gamehub-api/-/tree/master).

## [Lecture 10: Workshop - Continuous Integration](/10-ci-workshop)

This directory includes only a README detailing the expansion of the previous lecture's pipeline into including a [build stage](https://gitlab.com/malt9830/gamehub-api/-/blob/master/.gitlab-ci.yml).

## [Lecture 11: Languages, Databases, Storage and DNS](/11-stack)

This directory includes descriptions of the several assignments throughout lecture 11's 4 sub-lectures.

## [Lecture 12: Continuous Deployment](/12-cd)

This directory includes descriptions of the continuous deployment assignments using the [Gamehub API](https://gitlab.com/malt9830/gamehub-api/-/tree/master).