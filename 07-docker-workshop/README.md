# Lecture 7: Workshop - Dockerize Your App

Below is the assignment from the 7th lecture being the dockerization workshop.

[TOC]

## Assignments

### Assignment 7.1

_Dockerize a web application of your own choice. The project must fulfill these requirements:_

- _Your application must, at a minimum, be a three-tier web application, divided into a web server (reverse proxy), an application server, and a database server._
- _Your database must persistently store data in the database._
- _Your project must contain relevant custom Dockerfiles, Docker Compose files, environment files, any provisioning files, any scripts, and configuration files._
- _After an initial documented provisioning, it must be possible to start the project just running docker-compose up. The provisioning must only need to take place once._

I dockerized my Gamehub API using Express and MongoDB. The project, alongside the *Dockerfile*, *docker-compose.yml* and *nginx.conf* can be found [here](https://gitlab.com/malt9830/gamehub-api).