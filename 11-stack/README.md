# Lecture 11: Languages, Databases, Storage and DNS

Below are notes of and assignments from the 11th lecture, including those from [programming languages](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-11a/), [database systems](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-11b/), [block and object storage](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-11c/), as well as [DNS and certificates](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-11d/).

[TOC]

## Programming Languages

### Memory Management

The memory management of a language greatly impacts the security of code written in that language, based not on the language itself but rather the programmer using it.

Older languages like *C* and *C++* required the programmer to handle memory allocation and deallocation but modern languages typically handle these issues via [*garbage collection*](https://learn.microsoft.com/en-us/dotnet/standard/garbage-collection/fundamentals) or [*object reference counting*](https://learn.microsoft.com/en-us/windows/win32/com/rules-for-managing-reference-counts) to avoid security issues.

### Interpreted vs Compiled

For **interpreted languages**, the executable code is the source code itself. Examples include *JavaScript*, *Python* and *Ruby*.

**Compiled languages** require compilation into object files that are then linked into machine code. Examples include *C*, *C++* and *Rust*.

**JIT**, *Just in Time* compilation, is an optimization of interpreted languages, compiling them before execution. Examples include Mozilla's *SpiderMonkey* for JavaScript. 

### Paradigms

**Structured programming** languages have code organized into *procedures* and *functions* neither of which have a state, resulting in data being passed as arguments to the procedures and functions, emphasizing clear control flow. Examples include *C* and *Pascal*.

**Object-oriented programming** focuses on organizing code and data into *objects* containing state and behaviours. Examples include *JavaScript*, *Java*, *C#* and *C++*.

**Functional programming** revolves around code being organized into functions that do not have *side-effects* and only return data. Examples include *Lisp* and *Haskell*.

**Multi-paradigm** programming languages have strong support for several paradigms. Examples include *Python* and *Ruby*.

### Imperative vs Declarative

Most *general purpose languages* are **imperative languages** in that we give instructions like calling functions. Examples include *JavaScript*, *Python* and *C*.

Meanwhile **declarative languages** are based on descriptions of the outcome instead of how to achieve it. Examples include *SQL*, *HTML* and *CSS*

## Database Systems

### Paradigms

**Relational databases** organize data into rows and columns collectively forming a table where points of data are *related* to each other. They are often referred to as *SQL* databases as it is the language used by most relational databases. Furthermore all relational databases require a *schema* defining the tables, columns, constraints etc. Examples include *MySQL* and *PostgreSQL*.

While not a paradigm itself, *NoSQL* is an umbrella term for several other paradigms identified by *not* being SQL. Below are listed the most common NoSQL paradigms.

**Key-value databases** contain a set of keys where every key is unique and points to a value. The most popular example is *Redis*.

**Document databases** store data in documents, typically JSON, and lack a schema which allows for more flexibility at the potential cost of consistency. The most popular example is *MongoDB*.

**Graph databases** are based on *graph theory* and consists of *vertices* and *edges* where a vertices are entities within the graph and edges are the links between them. The most popular examples include *ArangoDB* and *Neo4j*.

### Object Relational Mapper

The **Object relational mapper**, abbreviated *ORM*, is an object-oriented tool providing a layer between relational databases and object-oriented programming languages translating the data into objects.

### ACID and BASE

One of core differences between SQL and NoSQL databases are their guarantees.

SQL databases are typically **ACID** compliant making them very reliable:

- Atomic: all operations in a transaction must be succesful or none will
- Consistent: each transaction leaves the database structurally intact
- Isolated: transactions take place in isolation from one another, appearing to run sequentially
- Durable: effects of transactions are permanent regardless of database failures

NoSQL databases are usually **BASE** compliant making them less reliable:

- Basically available: the database is accessible for concurrent users
- Soft state: lacking immediate consistency, data values may change over time
- Eventually consistent: while immediate consistency is not enforced, reads are still allowed even though they may not reflect reality

### Vertical vs Horizontal Scaling

While BASE offers less reliability it makes up for it in terms of scaling. ACID databases and their reliability have better **vertical scaling**, i.e. improving the server, but they lack moreso in **horizontal scaling**, i.e. expanding the amount of servers, which BASE excels at.

### CAP

**CAP** is a theorem refering to the principles of:

- Consistency: any client will see the same data regardless of the node they connect to
- Availability: there will be always be a response given there are working nodes
- Partition tolerance: the system functions even if there are communication breakdown between parts

Usually there will have to be prioritized 2 of the 3 elements, as you cannot guarantee them all.

## Block and Object Storage

### Block Storage

**Block storage** divides data into blocks with a unique identifier and accesses it through this identifier via a lookup table, making it able to rapidly find the data. Additionally, it has decent scalability as new blocks can be added when needed and when modifying you only need to modify the specific block affected when changing a file. However, the primary disadvantage of block storage is the fact that it is expensive. Its use cases includes critical system data and database storage.

### Object Storage

**Object storage** has data stored in isolated containers called objects, dividing data into separate units with unique identifiers and metadata making it easier to access and retrieve. Object storage has high scalability, simply adding more devices as you go, and it is built for storing high volumes of data. While it has slower performance, it is also cheap, paying only for the required storage with the ability to downscale based on your needs, reducing costs. Its use cases include assets, logs, media, etc.

## DNS and Certificates

### Domain Name System

The **Domain Name System**, abbreviated *DNS*, is a naming system allowing for systems to be accessed by *names* rather than their *IP addresses*, i.e. `kea.dk` instead of `89.34.18.61`. Additionally, DNS provides several types of records, such as *A*, *SOA*, *MX* and *TXT* each with their own purpose.

**A records**, i.e. *address records*, point to the domain name, e.g. for `kea.dk`, `A` points to the IP address above.

**SOA records**, i.e. *State of Authority records*, *must* detail administrative responsible contacts, that being who to contact in case of a problem.

**MX records**, i.e. *mail exchange records*, list mail servers responsible of how to handle emails on the domain, including a priority number, whereby the lower number indicates higher responsibility.

**TXT records**, i.e. *text records*, contain arbitrary text information, such as to prove ownership of a domain.

### Certificates

**HTTP** used to be the standard protocol of most internet traffic, but as security become more important on the internet, it evolved into **HTTPS**. However, this protocol requires a *certificate*, i.e. a certification issued to a host on an IP address verifying its legitimacy and preventing scams.

## Assignments

_For the assignments below, choose a web development project you have been involved in recently._

### Assignment 11a.1

_Who made the choice of programming language, and how was that decision made?_

For my multimedia design final semester project, my group made a JustEat clone in collaboration with the company that we all had been interning in during the first half of the semester. As such, the language chosen with JavaScript using the Vue.js framework, as this was the framework already in use for the rest of the company's projects, the primary one being a simplified website builder.

Vue was chosen due to its reactive values. Although those would also be possible in React and Angular, Vue was preferred due to it being more lightweight, as well as it being less corporative and more community focused, while React and Angular are made by *Meta* and *Google*, respectively.

### Assignment 11a.2

_In your experience, was the choice of language the right choice? Why or why not?_

As the company already was using Vue for their primary project, utilizing the same stack for this other project, that would be potentially be handed over to them in the future, would facilitate compatibility, make it easier for the developers to take over the project and allow for knowledge sharing between projects.

Additionally, since we had been learning Vue as interns working on their primary project, having prior experience with the framework helped streamline project development and reduce the learning curve.

Furthermore, as previously mentioned, Vue is independent and community-driven, unlike React and Angular, alleviating the risk of sudden changes in terms of licensing, ownership and corporate strategies, while the community focus allows for prioritization of inclusivity and transparency.

### Assignment 11a.3

_Make a requirement specification for the choice of programming language in the project, and evaluate five programming languages against the requirements._

The requirement specification can be found [here](https://docs.google.com/spreadsheets/d/1zRUIYke_p5LE9tDvMoFSc8yJcoUr2McqhmCE3cHGrBk/edit#gid=976367869&range=A2) and consists of general language and framework requirements as well as project-oriented ones. For languages/frameworks to compare I have gone with JavaScript using Vue, using React and Angular as well as Python with Django and Ruby on Rails. 

#### JavaScript with Vue

Vue uses an open-source licence while being independent and community-driven, thereby having a strong an active community and great ecosystem. Additionally, the learning curve while being smooth, is furthermore eased by the developers having previous experience with the framework as well as the framework already being used for the company's other project.

Regarding security, Vue provides several mechanisms to increase security such as automatic escaping, while also detailing best practices and rules for enforcing maximum security in their documentation. Vue also allows for the use of TypeScript for type safety.

Being a front-end framework, Vue provides a high level of interactivity and dynamicity in terms of user interface creation and responsiveness. Likewise, Vue may not be a full-stack framework, but communicates well with APIs through HTTP requests

#### JavaScript with React

React is very similar to the points above regarding Vue, but has some differences. The learning curve is generally considered to be slightly more difficult for React, while also the development team is more familiar with Vue and the company's primary project was made with Vue.

Additionally, React is corporately owned by Meta instead of being independent like Vue. Being developed and backed by a tech giant however creates a somewhat larger ecosystem.

#### JavaScript with Angular

Likewise, Angular offers much of the same as React and Vue, but again differs somewhat only in terms of learning curve compared to React, that being Angular's slightly steeper learning curve.


#### Python with Django

Django differs highly from the first 3 frameworks, as it is not a JavaScript framework and not a primarily front-end oriented framework. It does however have in common with Vue the independent ownership and community driven nature, while it has common in with React and Angular, a vast ecosystem.

Regarding the front-end, Django may suffer compared to the ease of dynamic user interface creation, styling and theming in the 3 previously mentioned frameworks. It does however provide capabilities for creating such interfaces and dynamic pages, while beit not as advanced and rich as that of the JavaScript frameworks.

For the back-end however, is where Django excels comparatively, being primarily a back-end framework providing strong support for data modeling, authentication and request handling.

#### Ruby on Rails

Similar to above, Ruby on Rails is independent, community driven and open-source albeit with a slightly smaller community and ecosystem compared to the 4 above, while pertaining a comparatively smooth learning curve.

Like Django, Ruby on Rails may fall behind regarding the front-end oriented technologies and ease of interface creation and dynamicity, while excelling at back-end in the same capabilities as Django above.


### Assignment 11b.1

_Analyze which database systems would be the best choice for a) accounts in a bank, b) a world-wide e-commerce system, c) route planning, d) movie recommendation system, e) a blog system, f) an email system, g) Project Gutenberg, h) IMDB, i) StackOverflow._

**a) Accounts in a bank**

It would be a must to have an SQL database due to their strong ACID compliance, regarding transactions from account to account, e.g. all parts of a bank transfer must be performed, multiple transfers cannot occur simultaneously and an overall consistency is crucial.

**b) A world-wide e-commerce system**

It might be a combination of both, using relational databases and their strong ACID compliancy for transactions based on the same logic as the previous answer, while a document database may be better for its flexibility and scalability regarding the product catalogue.

**c) Route planning**

For a route planning system, it might lean towards an SQL database as it may provide great functionality through its complex queries that may help with generating best possible route.

However a Graph database may also be beneficial for the purpose of modeling interconnected roads, streets, etc.

**d) Movie recommendation system**

A movie recommendation system may take advantage of a Graph based database to generate recommendations based on the relations between directors, actors, movies, etc. to rank potential movies for the user based on their liked movies, shows and potentially actors or directors.

**e) A blog system**

Blog systems typically have a lot of structured data from the posts themselves to comments and ratings, which altogether may lean more towards SQL databases.

**f) An email system**

An email system would require the high level of integrity and reliability that would come from relational databases in terms storing and delivering emails.

**g) Project Gutenberg**

Project Gutenberg digitalizes and archives cultural works and thus store high levels of text, which is one of the benefits from document databases.

**h) IMDb**

IMDb involves managing a lot of related information regarding movies and shows, therein actors, directors, episodes, etc. as well as user provided data such as reviews, rating, comments, etc. Achieving this may be easier with a relational database due to the complex queries that may be required.

**i) StackOverflow**

StackOverflow would be very similar to IMDB in terms of the amount of different data required in terms of users, questions, answers, comments, etc. that would have to come from complex queries.