# Lecture 1: Introduction, Vim and Tmux

Below are the commands learned from running Vimtutor, divided into sections based on purpose.

[Vim Cheat Sheet](https://vim.rtorr.com/) for more Vim commands, and likewise for Tmux commands in the [Tmux Cheat Sheet](https://tmuxcheatsheet.com/).

NB: `kea-dev` uses `CTRL+S` instead of Tmux Cheat Sheet's `CTRL+B`.

[TOC]

## Directions

| Description | Command |
| - | :-: |
| Left | `h` |
| Right | `l` |
| Up | `k` |
| Down | `j` |

## Cursor and File Location

| Description | Command |
| - | :-: |
| Show location in file and file status | `CTRL` + `g` |
| To beginning of the file | `gg` |
| To end of the file | `G` |
| Search | `/` + `<query>` |
| Search backwards | `?` + `<query>` |
| While searching, repeat in same direction | `n` |
| While searching, repeat in reverse direction | `N` |
| To older position in jump list | `CTRL` + `o` |
| To newer position in jump list | `CTRL` + `i` |
| While on `(`, `[` or `{`, go to matching parentheses | `%` |
| Substitute query with new text in line | `:s/<old>/<new>/g` |
| Substitute query with new text in line range | `:<from>,<to>s/<old>/<new>/g` |
| Substitute query with new text in file | `:%s/<old>/<new>/g` |
| Substitute query with new text promting each instance in sequence | `:%s/<old>/<new>/gc` |

## Motions

| Description | Command |
| - | :-: |
| To beginning of next word | `w` |
| To end of current word | `e` |
| To end of line | `$` |
| To beginning of line | `0` |

## Text Editing

| Description | Command |
| - | :-: |
| Delete character on cursor | `x` |
| Enter insertion mode on cursor | `i` |
| Enter insertion mode on character after cursor | `a` |
| Move to end of line and enter insertion mode | `A` |
| Move to line below and enter insertion mode | `o` |
| Move to line above and enter insertion mode | `O` |
| Copy character or selection | `y`
| Delete with motion | `d` + `<motion>` |
| Delete with count and motion | `d` + `<number>` + `<motion>` |
| Delete from cursor to beginning of next word | `d` + `w` |
| Delete from cursor to end of current word | `d` + `e` |
| Delete from cursor to end of line | `d` + `$` |
| Delete from cursor to beginning of line | `d` + `0` |
| Delete entire current line | `d` + `d` |
| Paste last deletion unto following line | `p` |
| Undo last command | `u` |
| Undo entire line to last changed state | `U` |
| Redo command | `CTRL` + `r` |
| Replace character on cursor | `r` + `<character>` |
| Enter replace mode until `<ESC>` | `R` |
| Delete with motion and enter insertion mode | `c` + `<motion>` |

## File Editing

| Description | Command |
| - | :-: |
| Quit | `:q` |
| Write | `:w` |
| Trash and quit | `:q!` |
| Write and quit | `:wq` |
| Save selection with visual mode | `v` + `<motion>` + `:w` |
| Retrieves FILENAME and puts on cursor | `:r` |
| Retrieves dir output and puts on cursor | `:r !dir` |
| Execute external command | `:!` + `<command>` |

## Set Options

| Description | Command |
| - | :-: |
| Set `ignorecase` to ignore case sensitivity | `:set ic` |
| Set `incsearch` to show partial matches for search queries | `:set is` |
| Set `hlsearch` to highlight matching phrases | `:set hls` |
| Preprend `no` to toggle off an option | `:set noic`, `:set nois`, `:set nohls` |

## Help

| Description | Command |
| - | :-: |
| Get help or description for a command | `:help <command>` |
