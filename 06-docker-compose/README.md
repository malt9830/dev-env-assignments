# Lecture 6: Virtualization and Process Isolation - Part II

Below are descriptions used Docker compose command, as well as the simple `docker-compose-demo` directory from the [lecture](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-06/) and the more in-depth example `helloweb` from class and the [video series](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-06/#video-lectures-containerize-your-app). At the bottom are descriptions of the 2 assignments.


[TOC]

## Docker Compose demo

The [lecture-based](/06-docker/docker-compose-demo) demo uses a simple `docker-compose.yml` running a single container with an *nginx* service.

## Helloweb demo

The [helloweb](/06-docker/helloweb) demo is a more layered project running three containers, running a container for the app server this being the `hello.py` using our own image built from our `Dockerfile`; the database server using the `postgres:14-alpine` image; and finally the web server using the latest `nginx` image.

## Docker Compose Commands

The list below includes the Docker Compose commands used during the [5th lecture's documentation](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-06/).

| Description | Command |
| - | :-: |
| Run multiple containers using their settings for ports, volumes, env variables, start up, etc. | `docker compose up` |
| Run the multiple containers like above but in detached mode | `docker compose up -d` |
| Stop running the containers | `docker compose down` |

## Assignments

### Assignment 6.1

_Complete the tutorial from Part I of this lecture._

The tutorial and its description can be found in the previous week's [assignments](/05-docker#assignment-51) and the associated repository using during the tutorial [here](https://gitlab.com/malt9830/docker-todo).

### Assignment 6.2

_Make a requirement specification for a process isolation system._

The requirements specification for the process isolation system can be found [here](https://docs.google.com/spreadsheets/d/1zRUIYke_p5LE9tDvMoFSc8yJcoUr2McqhmCE3cHGrBk/edit#gid=524018973&range=A2) and is filled out using the template from lecture 2.
