import multiply from '../src/multiply'
import { SingleDigit } from '../types'

const multiplyData: Array<[SingleDigit, SingleDigit, SingleDigit, number]> = [
  [1, 1, 1, 1], // All 1s
  [0, 0, 0, 0], // Only 0s
  [1, 2, 3, 6], // Lower boundary
  [7, 8, 9, 504], // Upper boundary
  [1, 5, 9, 45], // Lower, middle and upper
]

const doubleData: Array<[number, number, number]> = [
  [10, 1, 1], // First is double
  [1, 10, 1], // Second is double
  [1, 1, 10], // Last is double
  [10, 1, 10], // Two doubles
  [10, 10, 10], // All doubles
]

const negativeData: Array<[number, number, number]> = [
  [-1, 1, 1], // First is negative
  [1, -1, 1], // Second is negative
  [1, 1, -1], // Last is negative
  [-10, 1, -10], // Two negatives
  [-10, -10, -10], // All negatives
]

const decimalData: Array<[number, number, number]> = [
  [1.1, 1, 1], // First is negative
  [1, 1.1, 1], // Second is negative
  [1, 1, 1.1], // Last is negative
  [1.1, 1, 1.1], // Two negatives
  [1.1, 1.1, 1.1], // All negatives
]

describe('multiply', () => {
  it.each(multiplyData)('returns the correct products', (num1, num2, num3, exp) => {
    const product = multiply(num1, num2, num3)

    expect(product).toBe(exp)
  })

  it.each(doubleData)('should throw an error when using double digits', (num1, num2, num3) => {
    // @ts-expect-error
    expect(() => multiply(num1, num2, num3)).toThrow('Input must be single-digit.')
  })

  it.each(negativeData)('should throw an error when using negatives', (num1, num2, num3) => {
    // @ts-expect-error
    expect(() => multiply(num1, num2, num3)).toThrow('Input must be positive.')
  })

  it.each(decimalData)('should throw an error when using decimals', (num1, num2, num3) => {
    // @ts-expect-error
    expect(() => multiply(num1, num2, num3)).toThrow('Input must be an integer.')
  })
})
