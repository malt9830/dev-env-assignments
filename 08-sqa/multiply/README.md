# Multiply

The `multiply` function receives as parameters 3 positive single-digit integers and returns their product, and is built with TypeScript.

## Linting

For linting, the project uses [ESLint](https://eslint.org/) with the [config](/8-sqa/multiply/.eslintrc.js) defining the rules, i.e. 2 spaces for indentation, preference of single quotes and no end-of-line semi-colons. Additionally, it uses the `@typescript-eslint` plugin and its parser, while the `overrides` property contains configuration for used testing framework, Jest.

The linter can be run using `npm run lint` which is a package.json script for `eslint --fix . --ext .ts`.

## Testing

As mentioned above, the 4 unit tests were written using [Jest](https://jestjs.io/), each of them using their own data provider. Found [here](/8-sqa/multiply/tests/multiply.test.ts), the initial test simply tests the functionality with the provider including lower and upper boundary values, while also testing unique cases such as 0.

Using Jest's `toThrow` method, there are 3 separate test cases regarding the usage of *double-digited input*, *negative input* and *input with decimals* as these are not applicable parameters. It is then *expected* that each of these 3 tests will throw the appropriate error based on the input, e.g. `10`, `-1` or `1.1`.

The tests can be run using `npm run test` which is a package.json script for `jest`.

## Documentation

To extend beyond simple inline comments describing purpose of various if-statments, variables, etc., the [JSDoc](https://jsdoc.app/) documentation generator is used to provide better understanding as well as type declaration as seen below.

```
/**
 * Function receives 3 single-digit positive integers and returns their product
 *
 * @param {SingleDigit} num1 - single-digit positive integer
 * @param {SingleDigit} num2 - single-digit positive integer
 * @param {SingleDigit} num3 - single-digit positive integer
 * @returns { number } product of the 3 parameters
 */
```

Additionally, using the TypeScript extension of JSDoc, [TypeDoc](https://typedoc.org/), we can in seconds generate documentation of the function due to the code snippet below. 

The documentation can be generated using `npm run docs` which is a package.json script for `npx typedoc ./src/multiply.ts`. The output will by default be output to a `docs` directory.