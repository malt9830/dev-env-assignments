'use strict'

import { SingleDigit } from '../types'

/**
 * Function receives 3 single-digit positive integers and returns their product
 *
 * @param {SingleDigit} num1 - single-digit positive integer
 * @param {SingleDigit} num2 - single-digit positive integer
 * @param {SingleDigit} num3 - single-digit positive integer
 * @returns { number } product of the 3 parameters
 */
function multiply(num1: SingleDigit, num2: SingleDigit, num3: SingleDigit): number {
  const args: SingleDigit[] = [num1, num2, num3]
  const isNegative = (num: number): boolean => num < 0
  const isDouble = (num: number): boolean => num > 9
  const isDecimal = (num: number): boolean => !Number.isInteger(num)

  // Check if any params are double-digited
  if (args.some((num) => isDouble(num))) {
    throw new Error('Input must be single-digit.')
  }

  // Check if any params are negative
  if (args.some((num) => isNegative(num))) {
    throw new Error('Input must be positive.')
  }

  // Check if any params have decimals
  if (args.some((num) => isDecimal(num))) {
    throw new Error('Input must be an integer.')
  }

  return num1 * num2 * num3
}

export default multiply
