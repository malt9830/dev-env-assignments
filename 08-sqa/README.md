# Lecture 8: Software Quality Assurance

Below are assignments from the 8rd lecture about software quality assurance.

[TOC]

## Assignments

### Assignment 8.1

_Think of software projects you have done in school or maybe at work. List Software Quality Assurance measures taken in these projects. Do you think they were adequate? Do you think they were worth while? What steps would you take to apply quality assurance?_

Throughout projects, my most commonly used SQA tool would be *ESLint*, especially combined with *Prettier*, which have been used in more or less everything I have created.

However, I have also used testing frameworks like *Vitest* and *Cypress* for my multimedia design final semester project made in Vue.js creating *unit tests*, *component tests* and *end-to-end tests* as part of a testing elective. The unit tests were furthermore carried out by a *CI pipeline*, that triggered upon pushes to the branch.

Additionally, I also the static analysis tool, *SonarQube*, for the same project to detect issues such as vulnerabilities.

### Assignment 8.2

_Do some research about lint tools for your favorite programming language. Install a lint tool, and try it out with some test code. Work in the terminal environment for now, rather than an integrated development environment._

Implemented a simple console.log() script and used npm to install ESLint in [this project](/08-sqa/linting-demo), including an `.eslintrc.js` config and an `eslint --fix .` script in the `package.json`.

### Assignment 8.3a

_Do some research about testing tools and frameworks in for your favorite programming language (or just some other language than Python). What tools are available?_

For JavaScript the primary unit testing framework is [Jest](https://jestjs.io/), which is further extended by the almost identical [Vitest](https://vitest.dev/) incorporating more or less the exact same syntax as Jest but better suited for Vite.js projects, as Vitest is effectively Vite-flavoured Jest.

Complementarily, [Cypress](https://www.cypress.io/) can be combined with Vitest for a complete test suite, as it adds the possibility of not only automated end-to-end tests for your applications but also component testing for your Vue or React components.

### Assignment 8.3b

_Write up a few unit tests._

Some examples of unit tests can be found in the [project](/08-sqa/multiply/tests/multiply.test.ts) from [assignment 8.5](/8-sqa#assignment-85). There are built using Jest with TypeScript and use data providers in combination with the `it.each()` method and have cases that both simply expect the correct value as well as expect a certain error to be thrown.

### Assignment 8.4

_What are the standards of documentation in your favorite programming language?_

*Commenting* is commonplace in JavaScript, explaining the purpose, functionality and logic of code in immediate presence of it. Additionally, the API documentation generator, [JSDoc](https://jsdoc.app/), is also an established documentation method allowing for an enhanced way of writing inline comments to document code.

Outside of commenting, we have `README.md` files that also may serve the purpose of documenting code in a higher level overview, and is likely to also include the installation, setup and run steps of the code as well as usage snippets and examples.

Furthermore, companies such as *Google*, provide a JavaScript [style guide](https://google.github.io/styleguide/jsguide.html), explaining their own coding standards for their source code written in JavaScript.

### Assignment 8.5

_Write a function in your programming language of choice that takes three single-digit numbers as input and returns the product. Apply all elements of software quality assurance._

The [project](/08-sqa/multiply) uses the tools *ESLint*, *Jest* and *JSDoc*. Further descriptions and usage of these tools, as well as the method itself, is found in the project's README.