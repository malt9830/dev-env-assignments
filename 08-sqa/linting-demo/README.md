# Linting demo

Created a js script simply performing a console.log() and installed ESLint to enforce the coding standard.

```javascript
#!/usr/bin/env node

console.log('Yo!')
```

The rules of the lint config are descriped below with the purpose of enforcing the usage of *single quotes*, *no lind end semi colons* and *usage of 2 instead of 4 spaces for indentation*.

```json
'rules': {
        'indent': [
            'error',
            4
        ],
        'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'error',
            'never'
        ]
    }
```

Additionally, I added an ESLint script to the `package.json` to easily run the linter and fix any fixable warnings and errors.

```
    "lint": "eslint --fix .",
```