# Lecture 4: Version Control Systems

Below are descriptions of Git Demo from class and requirement specification assignment.

[TOC]

##  Git Demo

The `git-demo` contains the demo from class, also described in the [_Git by Example_](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-04/#git-by-example) section.


## Assignments

### 4.1 Assignment

_What do you need from a modern version control system? Make a requirement specification._

The requirements specification for the version control system can be found [here](https://docs.google.com/spreadsheets/d/1zRUIYke_p5LE9tDvMoFSc8yJcoUr2McqhmCE3cHGrBk/edit#gid=1220396549&range=A1) and is filled out using the template from lecture 2.

