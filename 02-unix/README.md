# Lecture 2: UNIX

Below are descriptions of the 6 assignments from the 2nd lecture.

[TOC]

## Assignments

### Assignment 2.1

_Find information on the `lab` command. Build a script that will download all your GitLab repositories to a backup folder._

The script is found in `backup-gitlab` and uses zaquestion's [lab](https://github.com/zaquestion/lab) as a wrapper for GitLab commands.

It fetches list of personal projects with `lab project list -m` and then iterates over each project checking if the clone already exists. If it does, it enters the directory and runs `git pull`; if not, it runs `lab clone` to create a new directory. 


### Assignment 2.2

_Write a script `quif`, that writes template files to the current directory. If given the command `quif index.html`, it should copy an `.html` template (say, from ~/.quif/templates/html) and name it `index.html`. The user must be able to add new templates by simply putting a file in the template directory._

The script is found in `quif` and uses a hidden directory *.quif* in the home directory to store templates.

When the script is run, the extension of the parameter is retrieved and matched against templates in the *.quif* directory, copying the template into the current directory if it exists.

### Assignment 2.3

_Create a script snipet that sources individual scripts from a folder `~/scripts/` for different hosts; say I have two machines `kea-dev` and `minime`, I would put two script files `kea-dev` and `minime` in the `~/scripts/` folder, and when the snipet is run on `kea-dev` it would source the `kea-dev` script (only), and likewise for `minime`. This would allow reusing your `.zshrc` file while still allow for differences between different machines and operating systems._

The script is found in `snippet` and when run simply enter the `~/scripts/` directory and runs the script by name of the host machine retrieved with `uname -n`.

### Assignment 2.4

_Create a script or function that 1) creates a directory with a given name, 2) runs `git init` in that directory, 3) creates a `README`.md file with some basic information about you, and 4) pushes the folder as a new project to GitLab. See `git push --set-upstream` for inspiration._

The script is found in `create-repo` and takes a parameter of the desired directory name.

When run, it converts the possible parameter to lowercase and replaces any spaces with dashes, if possible. Then it creates a directory by the name of this value, whereafter it enters it, outputs a simple *h1* and *p* to a `README`.

If the `README` was created, the script will continue *initialising* a git repository, *adding* the content and *committing* it. Then a remote origin of the directory name is added and the commits will be *pushed*.

### Assignment 2.5

_Take a look at https://www.soimort.org/translate-shell/. Based on this tool, try to make a flash card app, that displays a random word in one language, and then after a while shows the translation in your own language._

The script is found in `lingo-cards` and takes a language in the form of a string as parameter. Additionally it uses the `lingo-list.txt` file for words to use, and the *translate-shell* package for translations.

When run, the script shuffles the list of words and selects the first one. The word is then translated into the provided language and displayed in a box created with the `generateBox()` method. 3 seconds later, the word will be displayed in English.

`generateBox()` takes a word as a parameter and retrieves the length of this word with `${#TEXT}`. Then the box is created using simple ASCII double-pipes.

There will always be 2 spaces on each side the word, meaning the top and bottom row iterates over the word count plus 4, i.e. 2 for each side. For each iteration a middle double-pipe is created, so that the box is properly aligned.

### Assignment 2.6

_Create a Guess the number game. The user must guess a number between 1 and 100, and is told if the guess is too high or too low. Keep track of high-scores._

The script is found in `number-guesser` and generates a random number from 1 to 100 using the `shuf -i 1-100 -n 1` command. Additionally the script uses the `number-guesser-high-score` file to maintain a list of records.

Then a `while` based on a boolean is used to keep the game running until the number is guessed the value is set to true. Each guess increments a counter *attempts* by 1 and validates the input against answer.

If the guess is too low, the script will echo `$GUESS is too low!`, or if it is too high `$GUESS is too high!`.

When the number is guessed correctly, the amount of attempts needed will be echoed and the player will be asked to input their name via `read`, whereafter the used attempts and player name will be appended to the separate high score list which is then sorted numerically via `sort -n`.
