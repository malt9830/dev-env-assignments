# Lecture 5: Virtualization and Process Isolation

Below are descriptions of several commonly used Docker commands, flags and directives, while the `docker-demo` directory is the demo created during class. At the bottom are descriptions of the 2 assignments.

[TOC]

##  Docker Demo

The `docker-demo` contains the demo from class, also described in the [_Dockerfile, Images, and Containers_](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-05/#dockerfile-images-and-containers) section.


## Docker Commands

The list below includes Docker commands used during the [5th lecture's documentation](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-05/) as well as some commands used during class but not in the documentation.

| Description | Command |
| - | :-: |
| Run interactive container from Docker image | `docker run -it <id> sh` |
| Stop running Docker container by id | `docker container stop <id>`, `docker stop <id>` |
| Build Docker image from Dockerfile | `docker build .` |
| Build Docker image from Dockerfile with tag | `docker build -t <tag> .` |
| Start a shell session in a running container | `docker exec -it <name> sh` |
| List Docker containers | `docker container ls` |
| List Docker images | `docker image ls` |
| Remove Docker images tagged *<none>* | `docker image prune` |

## Docker Flags

| Description | Command |
| - | :-: |
| Runs the container in **detached** mode, i.e. in the background | `-d` |
| Runs the container in **interactive** mode, i.e. your input is transferred to the container | `-i` |
| Runs the container in **teletype** mode, i.e. your container's output is attached to your terminal | `-t` |
| Runs the container mapping port of host to port of container | `-p <port>:<port>` |
| Build the container with a given tag | `-t <tag>` |

## Docker Directives

The list below are the Docker directives listed in the [lecture](https://keaorg.gitlab.io/kea-dev-env-2024-1/lectures/lecture-05/#dockerfile-images-and-containers), and [here](https://docs.docker.com/reference/dockerfile/) is the complete list.

| Description | Directive |
| - | :-: |
| Specify a base image | `FROM` |
| A command to run when the container is deployed | `CMD` |
| Command to run during build | `RUN` |
| Expose a network port to the Docker network | `EXPOSE` |
| Copy files from host machine to image | `COPY` |
| Copy or download assets to the image | `ADD` |
| Make the container run a command as an executable | `ENTRYPOINT` |
| Specify working directory | `WORKDIR` |
| Specify environment variables | `ENV` |

## Assignments

### Assignment 5.1

_Follow the steps below. Explain what is going on - use documentation as needed._

```
$ ssh kea-dev

kea-dev:~$ mkdir docker101/
kea-dev:~$ cd docker101/
kea-dev:~/docker101$ ls
kea-dev:~/docker101$ docker run -dp 8000:80 docker/getting-started
```
By running the command `docker run -dp 8000:80 docker/getting-started`, we start a container in detached mode via `-d` and map the port 8000 of the host to port 80 in the container via `-p` followed by the port mapping using the image `docker/getting-started`.

The repository of the *getting-started* todo-app used for this assignment can be found [here](https://gitlab.com/malt9830/docker-todo).

#### Getting Started

Docker maintains a desktop app [*Docker Dashboard*](https://docs.docker.com/desktop/) giving a quick overview of containers currently running on your machine as well as allowing you to easily manage a container's lifecycle, while additionally providing you with container logs and enables you to access the shell inside the container.

**Containers** are processes on your machine that have been isolated from any other processes on the host machine. They are lightweight and contain everything needed to run the application in its filesystem, therefore not relying on anything installed on the host, effetively eliminating the concept of *something working on your machine but not on your colleague's machine*.

An **image** is a read-only template used to create a Docker container and thus provides the custom filesystem mentioned above. This means the image must contain all dependencies, configuration, scripts, binaries, etc. required to run the application,  as well as any environment variables, metadata or run commands.

#### Our Application

Having downloaded the zip file and unpackaged it, we create this [`Dockerfile`](05-docker/getting-started/Dockerfile) in the root of the project. We then build an image tagged *getting-started* via `docker build -t getting-started .` and run the container on our port 3000 via `docker run -dp 3000:3000 getting-started` in detached mode as seen below in the desktop app.

![container running](05-docker/getting-started/container-running.png)

#### Updating our App

After changing a line of text in our app, we build a new image again through `docker build -t getting-started .` and attempt to run it through the same `docker run -dp 3000:3000 getting-started`, but receive an error as our previous container is still running on the given port.

To stop the old container we use `docker ps` to see a list of containers and their IDs. Knowing the ID of the container we run `docker stop <id>` followed by `docker rm <id>` to remove the container after we stop it.

Now we run the container again via the above command to see the change we made.

#### Sharing our App

Next we visit *Docker Hub* and create a new public repository *getting-started*. However when attempting to push our image via `docker push <username>/getting-started`, a local image cannot be found. We will fix this by tagging our image, but first we need to sign in via `docker login -u <username>`.

Once we are signed in to our Docker Hub account, we tag our image with `docker tag getting-started <username>/getting-started` and repeat the push command from above, this time succesfully pushing our latest image to Docker Hub.

Now that the image is published, we attempt to run our app in a brand new instance using [*Play with Docker*](https://labs.play-with-docker.com/). Once we have logged in, we start a new container in the terminal using our image via `docker run -dp 3000:3000 <username>/getting-started`, which starts pulling our latest image.

Once the image is pulled and the container is running, we click *Open Port* at the top of *Play with Docker* and enter 3000, since we mapped to this port when the container was run and here we see our todo app.

### Assignment 5.2

_Try to run some of the GUI applications from the [link in Not Just Terminal App](https://betterprogramming.pub/running-desktop-apps-in-docker-43a70a5265c4) on your local computer (if you can run Docker on your machine)._

`TODO`
